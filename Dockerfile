ARG   IMAGE_RUBY_VERSION=2.7.0-slim
FROM  ruby:${IMAGE_RUBY_VERSION}

# Build dependencies
RUN apt-get update -yq; apt-get -y install curl wget apt-transport-https build-essential software-properties-common unzip
# Node
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -; apt-get update -yq; apt-get install -y apt-transport-https build-essential cmake nodejs software-properties-common unzip
# Yarn
RUN wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -; echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list; apt-get update -yq; apt-get install -y yarn
# Chromedriver
RUN apt-get install -y ruby-chromedriver-helper
