image:
	docker build 	-t registry.gitlab.com/lipoqil/docker-ruby-chromedriver-node-yarn:ruby-${IMAGE_RUBY_VERSION} \
								-t mailo/ruby-chromedriver-node-yarn:ruby-${IMAGE_RUBY_VERSION} \
								--build-arg IMAGE_RUBY_VERSION=${IMAGE_RUBY_VERSION} \
								.

push:
	docker push registry.gitlab.com/lipoqil/docker-ruby-chromedriver-node-yarn:ruby-${IMAGE_RUBY_VERSION}
	docker push mailo/ruby-chromedriver-node-yarn:ruby-${IMAGE_RUBY_VERSION}

publish_new_image: image push
