# Ruby + Chromedriver + Node.js + Yarn

Image prepared to run feature specs on Gitlab CI

## Build custom Ruby version

Check images at https://hub.docker.com/_/ruby and then run something like this

~~~bash
IMAGE_RUBY_VERSION=2.7.0-slim make image
~~~
